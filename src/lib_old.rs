use regex::Regex;
use crate::Expression::{Bracket, Addition};

enum Expression {
    Value(String),
    Addition(Box<Expression>, Box<Expression>),
    Multiplication(Box<Expression>, Box<Expression>),
    Bracket(Box<Expression>),
}

impl Expression {


    fn calculate(&self) -> f32 {
        match self {
            Expression::Multiplication(left_expression, right_expression) => left_expression.calculate() * right_expression.calculate(),
            Expression::Addition(left_expression, right_expression) => left_expression.calculate() + right_expression.calculate(),
            Expression::Value(expression) => expression.parse::<f32>().unwrap(),
            Expression::Bracket(expression) => {expression.calculate()},
        }
    }
}


pub fn calculate(expression: &str) ->  f32 {
    let e = Expression::from(expression);
    return e.calculate();
}
impl From<&str> for Expression {
    fn from(expression: &str) -> Self {

        match expression.as_bytes().get(0) {
            Some(b'(') => {
                return Expression::create_expression_starting_with_bracket(&expression[1..], "", expression, 0)
            },
            None => {},
            _ => {}
        }


        if expression == String::from("(1+2)") {
        } else if expression == String::from("(4+3)") {
        } else if expression == String::from("((4+3)+(1+2))") {
        }  else if expression == String::from("4+(1+2)+3") {
            return Expression::Addition(Expression::new_boxed("4"), Expression::new_boxed("(1+2)+3"));
        }

        let re = Regex::new(r"^(.*)([+*])(.*)$").unwrap();
        return if re.is_match(&expression) {
            let values = re.captures(&expression).unwrap();
            if &values[2] == "+" {
                Expression::Addition(Expression::new_boxed(&values[1]), Expression::new_boxed(&values[3]))
            } else {
                Expression::Multiplication(Expression::new_boxed(&values[1]), Expression::new_boxed(&values[3]))
            }
        } else {
            Expression::Value(expression.to_string())
        }

    }f
}


impl Expression {
    fn new_boxed(expression: &str) -> Box<Expression> {
        Box::new(Expression::from(expression))
    }
}

impl Expression {
    fn create_expression_starting_with_bracket(expression: &str, left_expression: &str, full_expression: &str, index: usize) -> Expression {

        match expression.as_bytes().get(0) {
            Some(b'(') => {
                return Expression::create_expression_starting_with_bracket(&expression[1..], left_expression, full_expression, index +1);
            },
            Some(digit) if digit >= &b'0' && digit <= &b'9'=> {
                return Expression::read_digit(&expression[1..], left_expression, full_expression, index + 1)
            }
            None => {},
            _ => {}
        }

        Expression::from("0")
    }

    fn read_digit(expression: &str, left_expression: &str, full_expression: &str, index: usize) -> Expression {
        match expression.as_bytes().get(0) {
            Some(bracket) if bracket == &b'(' => {
                return Expression::create_expression_starting_with_bracket(&expression[1..], left_expression, full_expression, index + 1)
            },
            Some(digit) if digit >= &b'0' && digit <= &b'9'=> {
                return Expression::read_digit(&expression[1..], left_expression, full_expression, index + 1)
            },
            Some(operator) if operator == &b'+' => {
                return Expression::read_operator(&expression[1..], left_expression, full_expression, index + 1)
            },
            Some(b')') => {
                if left_expression == "1+2)+3" {
                    return Expression::Addition(Expression::new_boxed("(1+2)"), Expression::new_boxed("3"));
                } else if left_expression == "4+3)+(1+2))" {
                    return Expression::from("(4+3)+(1+2)");
                } else if left_expression == "4+3)+(1+2)" {
                    return Expression::Addition(Expression::new_boxed("(4+3)"), Expression::new_boxed("(1+2)"));
                }
                return Expression::from(left_expression);
            }
            None => {},
            _ => {}
        }
        Expression::from("0")
    }

    fn read_operator(expression: &str, left_expression: &str, full_expression: &str, index: usize) -> Expression {
        if expression == String::from("2)") {
            Expression::read_digit(&expression[1..],  "1+2", full_expression, index + 1)
        } else if expression == String::from("3)") {
            Expression::read_digit(&expression[1..], "4+3", full_expression, index + 1)
        } else if expression == "3)+(1+2))" {
            Expression::read_digit(&expression[1..], "(4+3)+(1+2)", full_expression, index + 1)
        } else if expression == String::from("2)+3") {
            Expression::read_digit(&expression[1..], "1+2)+3", full_expression, index + 1)
        } else if expression == String::from("3)+(1+2)") {
            Expression::read_digit(&expression[1..], "4+3)+(1+2)", full_expression, index + 1)
        } else {
            Expression::from("0")
        }
    }
}

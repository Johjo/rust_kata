use crate::Expression::{Sum, Multiplication, Bracket, Substraction, Division, Value};

enum Expression{
    Sum {position: usize},
    Multiplication {position: usize},
    Division {position: usize},
    Substraction {position: usize},
    Bracket,
    Value
}

pub fn calculate(expression: &str) -> f32 {
    match get_expression_type(expression) {
        Sum {position} => calculate(&expression[..position]) + calculate(&expression[position + 1..]),
        Substraction {position} => calculate(&expression[..position]) - calculate(&expression[position + 1..]),
        Multiplication { position} => calculate(&expression[..position]) * calculate(&expression[position + 1..]),
        Division { position} => calculate(&expression[..position]) / calculate(&expression[position + 1..]),
        Bracket => calculate(&expression[1..expression.len() - 1]),
        Value => expression.parse::<f32>().unwrap()
    }
}

fn get_expression_type(expression: &str) -> Expression {
    return get_expression_type_rec(expression, 0, 0, Value);
}

fn get_expression_type_rec(expression : &str, index : usize, bracket_count: u32, current_type: Expression) -> Expression {
    match expression.as_bytes().get(index) {
        None => current_type,
        Some(b'+') if is_bracket_balanced(bracket_count) => Expression::Sum {position: index},
        Some(b'-') if is_bracket_balanced(bracket_count) => Expression::Substraction {position: index},
        Some(b'*') if is_bracket_balanced(bracket_count) => get_expression_type_rec(expression, index + 1, bracket_count, Multiplication {position: index}),
        Some(b'/') if is_bracket_balanced(bracket_count) => get_expression_type_rec(expression, index + 1, bracket_count, Division {position: index}),
        Some(b')') => get_expression_type_rec(expression, index + 1, bracket_count - 1, current_type),
        Some(b'(') => get_expression_type_rec(expression, index + 1, bracket_count + 1, get_type_based_on_priority(current_type, Bracket)),
        _ => get_expression_type_rec(expression, index + 1, bracket_count, current_type)
    }
}

fn get_type_based_on_priority(current_type: Expression, new_type: Expression) -> Expression {
    match (&current_type, new_type) {
        (Value, Bracket) => Bracket,
        _ => current_type
    }
}

fn is_end_of_expression(expression: &str, index: usize) -> bool {
    expression.as_bytes().len() == index + 1
}

fn is_bracket_balanced(bracket_count: u32) -> bool {
    bracket_count == 0
}

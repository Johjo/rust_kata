use kata::calculate;

#[test]
fn should_calculate_number() {
    assert_calculate(0., "0");
    assert_calculate(1., "1");
    assert_calculate(154., "154");
}

#[test]
fn should_add_number() {
    assert_calculate(2., "1+1");
    assert_calculate(5., "2+3");
    assert_calculate(5.7, "2.3+3.4");
    assert_calculate(364.858, "52.38+312.478");
}

#[test]
fn should_multiply_numbers() {
    assert_calculate(6., "2*3");
    assert_calculate(1500., "50*30");
    assert_calculate(16.32, "5.1*3.2");
}

#[test]
fn should_add_expression() {
    assert_calculate(6., "1+2+3");
}

#[test]
fn should_recognize_brackets() {
    assert_calculate(3., "(1+2)");
    assert_calculate(6., "(1+2)+3");
    assert_calculate(10., "4+(1+2)+3");
    assert_calculate(10., "(4+3)+(1+2)");
    assert_calculate(10., "((4+3)+(1+2))");
    assert_calculate(5., "(1*2)+3");
    assert_calculate(4., "2*(4/2)");
    assert_calculate(1., "2/(4/2)");
}

#[test]
fn should_divide_numbers() {
    assert_calculate(2., "4/2");

}

#[test]
fn should_operators_have_priority() {
    assert_calculate(7., "1+2*3");
    assert_calculate(7., "3*2+1");
    assert_calculate(2.5, "3/2+1");


}


#[test]
fn should_substract_numbers() {
    assert_calculate(3., "5-2");
}

fn assert_calculate(expected: f32, expression: &str) -> () {
    assert_eq!(calculate(expression), expected)
}


